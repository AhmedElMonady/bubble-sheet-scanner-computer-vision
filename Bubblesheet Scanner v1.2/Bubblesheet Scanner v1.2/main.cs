﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace BubblesheetScannerV1_2
{
    public partial class main : Form
    {
        public static float width = Screen.PrimaryScreen.WorkingArea.Width;
        public static float height = Screen.PrimaryScreen.WorkingArea.Height;

        public static float weight = 0.45f;

        static string desktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

        static string path_img= "", img_filename, img_extension,img_filename_ext;
        static string path_python="", path_script="",script_filename;
        static string data_def = "";
        static string result = "";

        public main()
        {
            InitializeComponent();
            init_Form();
            init_PictureBox();
            init_Data_TextBox();
            init_Btns(); 
            init_Labels();
            init_OpenDialogs();
            //default values for reference
            path_img = @"no_image.jpg";
            data_def = "Waiting for: \r\n \t-python.exe \r\n \t-<script>.py \r\n \t-<image file>\r\n To be Attached.";

            this.SizeChanged += Form1_SizeChanged;
            this.Text = "Bubblesheet Scanner v1.2";
        }
        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            init_Form();
            init_PictureBox();
            init_Data_TextBox();
            init_Btns();
            init_Labels();

            pictureBox1.Image = Image.FromFile(path_img);
            data.Text = data_def;
        }
        bool go_empty_check()
        {
            if (path_python == "" || path_script == "" || path_img == "")
                return true;
            return false;
        }
    }
}
